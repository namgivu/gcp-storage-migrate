#!/usr/bin/env bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../" && pwd)
    KEYFILE_VAULT="$AH/gcp-storage-migrate.private/json-serviceaccount-keyfile/json_keyfile_vault"
    cd $AH
        KEYFILE_PROJECT1="$KEYFILE_VAULT/nntest.project1-289904-63b975bf7749.json" \
        KEYFILE_PROJECT2="$KEYFILE_VAULT/hoa-project2-gcloudstorage.json" \
        PYTHONPATH=$AH  pipenv run  python ./task/gcs_migrate_bucket.py
        #               .           .             .
