#!/usr/bin/env bash

SH=`cd $(dirname $BASH_SOURCE) && pwd`  # SH aka SCRIPT_HOME
AH=$(cd "$SH/../" && pwd)

# define to create backup bucket
BUCKET_LOCATION=ASIA-SOUTHEAST1  # ref. https://cloud.google.com/storage/docs/locations
BUCKET_MOVE=nnp1
PROJECT1_ID=project1-289904
PROJECT2_ID=scenic-doodad-290403

# JSON FILE: Make sure all your json files have storage admin access
JSON_KEY_PR1=$AH/key-file-project1.json
JSON_KEY_PR2=$AH/key-file-project2.json
CLIENT_EMAIL2=transer@scenic-doodad-290403.iam.gserviceaccount.com  # this is email of your service_account
