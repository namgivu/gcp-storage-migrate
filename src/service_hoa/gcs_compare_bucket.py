import sys
from google.cloud import storage


"""
Usage:
    pipenv run python gg_list_files.py :bucket_name1 :bucket_name2
"""


class Storage:

    @staticmethod
    def listing_all_files(bucket_name):
        """
        Make sure credential must have permission viewer on bucket
        Return to set of filename
        """
        storage_client = storage.Client()
        try:
            blobs = storage_client.list_blobs(bucket_name)
            if blobs:
                set_filename = set([blob.name for blob in blobs])
            else:
                set_filename = set()
            print(f'[GoogleApi][Storage][{bucket_name}] Files are listed: {len(set_filename)}')
            return set_filename
        except Exception as e:
            print('[GoogleApi][Storage] Error listing all files: ', e)
            raise


    @staticmethod
    def compare_two_bucket(bucket1, bucket2):
        set_bucket1 = Storage.listing_all_files(bucket1)
        set_bucket2 = Storage.listing_all_files(bucket2)
        print(f'Compare {bucket1} == {bucket2}: {set_bucket1 == set_bucket2}')
        return set_bucket1 == set_bucket2


if __name__ == '__main__':
    bucket_name1 = sys.argv[1]
    bucket_name2 = sys.argv[2]
    Storage.compare_two_bucket(bucket_name1, bucket_name2)
