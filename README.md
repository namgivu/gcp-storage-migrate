# Context
1. We have :nnp1-bucket in project1; nnp1-bucket aka nn's project1 bucket
2. We want to move :nnp1-bucket to project2
3. We want public url of files not changed after moved


# Main Idea
Bucket name is unique globally; public_url of files won't changed after migrated the bucket keeping same bucket name.
So it is 
```
migration bucket B == clone bucket B@p1 to B-backup@p1, then delete B, and then transfer B-backup to B@p2
#                     .            .       .                 .                  .
```
    
# STEP TO DO ** In brief **
00 clone/backup    `nnp1-bucket @ project1` to `nnp1-bucket-backup @ project1`
01 delete          `nnp1-bucket @ project1`
02 transfer/rename `nnp1-bucket-backup @ project1` to `nnp1-bucket @ project2`
03 publish all bucket files/blobs for `nnp1-bucket @ project2`
done

(optional)
compare file list between `nnp1-bucket-backup @ project1`
                       vs `nnp1-bucket        @ project2`
                                                                                
# STEP TO DO ** Full details **
1. Create nnp1-bucket-backup in project1
2. Create nnp1-bucket-copy   in project2

3. Backup nnp1-bucket as nnp1-bucket-backup in project1 via Transfer
   aka 
   Transfer        nnp1-bucket@project1 
         to nnp1-bucket-backup@project1
         
    3.1 Go to link: https://console.cloud.google.com/transfer/cloud
    3.2 Make sure we are in project1
    3.3 Create :transfer with
        - Source      : nnp1-bucket
        - Destination : nnp1-bucket-backup
    3.4 Run & wait for completion

4. Delete nnp1-bucket @ project1
5. Create nnp1-bucket @ project2
    
6. Copy nnp1-bucket-backup to nnp1-bucket, these steps similar to step 3.x with nnp1-bucket as Destination
   ie 
   Transfer nnp1-bucket-backup@project1 
         to nnp1-bucket@project2
        
(make bucket's files public)
7. Make sure we have credentials json file to run python script
    7.1 Create json if needed. 
        Go to link: https://console.cloud.google.com/apis/credentials/serviceaccountkey?_ga=2.142485108.774836035.1600944788-621870667.1600943992
    7.2 Add code below into .env
        export GOOGLE_APPLICATION_CREDENTIALS="path to credentials"
        
8. Run python file to make public to all files in bucket:
```
cd :gcp-storage-migrate/
    pipenv sync
    PYTHONPATH=:gcp-storage-migrate/  pipenv run python ./src/service_hoa/gcs_make_public.py nnp1-bucket
``` 

9. If any errors about permissions when transfering, do like one of steps below:
    9.1 Try to reload Storage/Browser and check Permission of Bucket must be updated permission: Security Admin
    9.2 Wait for 5mins to 10mins, duplicate new transfer page and go to Storage/Browser and check Permission of Bucket must be update permission: Security Admin
    9.3 *Noted: Security Admin permission will be updated by GCS, aka GCP Cloud Storage, when running transfer job, so we just need to check it's updated


#TODO step 7 and 9 need more clarification
