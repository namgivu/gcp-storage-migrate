from os import walk
from os.path import join

import sys
from google.cloud import storage


class GCS:  # GCS aka GoogleCloudStorage

    @classmethod
    def upload_folder(cls, bucket_name, local_folder, bucket_folder, verbose=True):
        """
        Upload local folder to bucket
        Required: Make sure folder must have at least one file
        """
        c = storage.Client()
        bucket = c.get_bucket(bucket_name)
        for root, sub_dir, files in walk(local_folder):
            bucket_folder_2 = bucket_folder
            if root != local_folder:
                sub_d = root.replace(local_folder, '')
                bucket_folder_2 = bucket_folder + sub_d + '/'
            for file in files:
                local_file = join(root, file)
                blob = bucket.blob(bucket_folder_2 + file)
                blob.upload_from_filename(local_file)
                blob.make_public()
                if verbose: print(f'[GoogleApi][Storage][{bucket_name}][{bucket_folder_2}] File uploaded: {file}')


if __name__ == '__main__':
    """
    **sampel usage**
    pipenv run python  :gcp-storage-migrate/src/gcloud_storage/upload_folder.py  nnp1-bucket   /tmp/img/      img/
    #                  .                                                         :bucket_name  :local_folder  :bucket_folder
    """

    bucket_name   = sys.argv[1]
    local_folder  = sys.argv[2]
    bucket_folder = sys.argv[3]

    GCS.upload_folder(bucket_name, local_folder, bucket_folder)
