import os
from os.path import dirname, abspath
from datetime import datetime

import src.service.gcs as GCS

"""
sample usage
./run.sh
"""

"""
# fixture prepare
prepare bucket nnp1-bucket-YmDHMS @ project1
with fixture set as tests/_fixture_/sample_bucket/
get public url of certain blob aka puburl - we expect after migrated, these :puburl still working
    

# steps
00  transfer/backup/clone `nnp1-bucket-YmDHMS        @ project1` to `nnp1-bucket-YmDHMS-backup @ project1`
02a delete                `nnp1-bucket-YmDHMS        @ project1`
02b create                `nnp1-bucket-YmDHMS        @ project2`
02c transfer/rename       `nnp1-bucket-YmDHMS-backup @ project2` to `nnp1-bucket-YmDHMS @ project2`
03  publish all files in  `nnp1-bucket-YmDHMS        @ project2`
04  check :puburl still working
done

# clean up
delete bucket nnp1-bucket-YmDHMS-backup @ project1
delete bucket nnp1-bucket-YmDHMS        @ project2
"""

#region envvar for json serviceaccount file
'''
We have TWO storage project :project1 and :project2 
so here we load the two keyfiles
'''
KEYFILE_PROJECT1=os.environ.get('KEYFILE_PROJECT1'); assert KEYFILE_PROJECT1
KEYFILE_PROJECT2=os.environ.get('KEYFILE_PROJECT2'); assert KEYFILE_PROJECT2
#endregion envvar for json serviceaccount file


def debug_exit():
    GCS.delete_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=KEYFILE_PROJECT1, skip_ifnotexists=True)
    GCS.delete_bucket(f'{BUCKET_NAME}',        from_service_account_json=KEYFILE_PROJECT2, skip_ifnotexists=True)
    import sys; sys.exit()


#region prepare nnp1-bucket-YmDHMS fixture @ project1
print('''\n---\nprepare nnp1-bucket-YmDHMS fixture @ project1''')

YmDHMS      = datetime.now().strftime('%Y%m%d-%H%M%S')  # timestamp
BUCKET_NAME = f'nnp1-bucket-{YmDHMS}'
print(f'GCS.create_bucket({BUCKET_NAME}, KEYFILE_PROJECT1) ...'); GCS.create_bucket(BUCKET_NAME, from_service_account_json=KEYFILE_PROJECT1)

print(f'GCS.upload_folder({BUCKET_NAME}, KEYFILE_PROJECT1) ...')
AH                 = abspath(dirname(__file__)+'/..')
FIXTURE_BUCKET_DIR = f'{AH}/tests/_fixture_/sample_bucket/'
GCS.upload_folder(from_local_folder=FIXTURE_BUCKET_DIR, to_bucket_name=BUCKET_NAME, to_bucket_folder='', from_service_account_json=KEYFILE_PROJECT1)
#endregion prepare nnp1-bucket-YmDHMS fixture @ project1


#region 00 create xx-backup
print('''\n---\n00 create xx-backup''')

print(f'''
backup `{BUCKET_NAME} @ project1` 
    to `{BUCKET_NAME}-backup @ project1`
''')

print(f'GCS.create_bucket({BUCKET_NAME}-backup, KEYFILE_PROJECT1');                                    GCS.create_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=KEYFILE_PROJECT1)
print(f'GCS.copy_bucket(from_bucket={BUCKET_NAME}, to_bucket={BUCKET_NAME}-backup, KEYFILE_PROJECT1'); GCS.copy_bucket(from_bucket=f'{BUCKET_NAME}', to_bucket=f'{BUCKET_NAME}-backup', json_serviceaccount_file=KEYFILE_PROJECT1)
#endregion 00 create xx-backup


#region 02a 02b delete@p1 rename@p2
print('''\n---\n02a 02b delete@p1 rename@p2''')

print(f'''
02a delete          `{BUCKET_NAME}       @ project1`
02b create          `{BUCKET_NAME}       @ project2`
02c transfer/rename `{BUCKET_NAME}-backup @ project2` to `{BUCKET_NAME} @ project2` - pls mind to SELECT project as :project2
''')
print(f'GCS.delete_bucket({BUCKET_NAME}, KEYFILE_PROJECT1) ...'); GCS.delete_bucket(f'{BUCKET_NAME}', from_service_account_json=KEYFILE_PROJECT1)
print(f'GCS.create_bucket({BUCKET_NAME}, KEYFILE_PROJECT2) ...'); GCS.create_bucket(f'{BUCKET_NAME}', from_service_account_json=KEYFILE_PROJECT2)
GCS.copy_bucket(from_bucket=f'{BUCKET_NAME}-backup', to_bucket=f'{BUCKET_NAME}', json_serviceaccount_file=KEYFILE_PROJECT2)  #TODO what json serviceaccount file for :from_bucket
print(f'GCS.delete_bucket({BUCKET_NAME}-backup, KEYFILE_PROJECT2) ...'); GCS.delete_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=KEYFILE_PROJECT2)
#endregion 02a 02b delete@p1 rename@p2

debug_exit()  #TODO remove this when done debugging


# 03  publish all bucket files in `nnp1-bucket-YmDHMS @ project2`
print(f'GCS.make_public({BUCKET_NAME}, ENV_PROJECT2)')
GCS.make_public(BUCKET_NAME, from_service_account_json=KEYFILE_PROJECT2)


input('''
---
gcs_migrate_bucket steps done here
Pls :enter to proceed ending...
''')


#region aftermath
'''
TODO ensure file list in       `nnp1-bucket-YmDHMS        @ project2`
                      equal to `nnp1-bucket-YmDHMS-backup @ project2`
                      equal to :FIXTURE_BUCKET_DIR
TODO test download a public url - both before/after the migrate with same url
'''
#endregion aftermath


#region clean up
print('''\n---\nclean up''')
print(f'GCS.delete_bucket({BUCKET_NAME}-backup, KEYFILE_PROJECT1) ...'); GCS.delete_bucket(f'{BUCKET_NAME}-backup', from_service_account_json=KEYFILE_PROJECT1)
print(f'GCS.delete_bucket({BUCKET_NAME},        KEYFILE_PROJECT2) ...'); GCS.delete_bucket(f'{BUCKET_NAME}',        from_service_account_json=KEYFILE_PROJECT2)
#endregion clean up
